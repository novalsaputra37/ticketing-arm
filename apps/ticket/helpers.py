from traceback import print_tb
from apps.ticket.models import ApprovementTicketModels


def create_approvement_models(ticket):
    checkers = ticket.checker.all()

    """ create ApprovementTicketModels from checkers tickets """
    for checker in checkers:
        ApprovementTicketModels.objects.create(
            user=checker,
            checker=ticket,
            approve=False
        )


def filter_update_checker_helper(ticket):
    checkers = ticket.checker.all()

    """ add or create checker in approvement models """
    checker_not_found_in_ticket_models = []

    """ if checker not found in ApprovementTicketModels then checker append in list """
    for checker in checkers:
        try:
            ApprovementTicketModels.objects.get(user=checker, checker__title=ticket.title)
        except:
            checker_not_found_in_ticket_models.append(checker)

    """ checker not found in ApprovementTicketModels will create new models """
    if checker_not_found_in_ticket_models is not None:
        for checker in checker_not_found_in_ticket_models:
            ApprovementTicketModels.objects.create(
                user=checker,
                checker=ticket,
                approve=False
            )

    """ delete checker in approvement models """
    approvement_objs = ApprovementTicketModels.objects.filter(checker__title=ticket.title)
    appr_users = []

    """ append user object """
    for approvement_obj in approvement_objs:
        appr_users.append(approvement_obj.user)

    """ remove user in list to find deleted data """
    for checker in checkers:
        if checker in appr_users:
            appr_users.remove(checker)

    """ Delete object in approvement models """
    for user in appr_users:
        ApprovementTicketModels.objects.get(user=user, checker__title=ticket.title).delete()


def massage_info_helper(self, form, ticket):
    """ send massage approver """
    try:
       list_info = ticket['info']
    except:
        list_info = []
    dict_info = {}

    user = self.request.user.username
    data_info = form.cleaned_data['info']

    dict_info["user"] = user
    dict_info["massage"] = data_info

    list_info.append(dict_info)
    data_dict = {'info': list_info}

    form.instance.info = data_dict
    form.save()


def filter_approve_ticket_helper(tickets):
    tickets_approved = []
    tickets_not_approve = []

    """ filter data approve and not approve """
    for ticket in tickets:
        if ticket.checker.title and ticket.approve:
            tickets_approved.append(ticket.checker.title)
        else:
            tickets_not_approve.append(ticket.checker.title)

    """ Delete duplicate title ticket """
    tickets_approved = list(dict.fromkeys(tickets_approved))
    tickets_not_approve = list(dict.fromkeys(tickets_not_approve))

    """ if ticket that has not been approved,
        then delete the ticket title in the ticket_approved list """
    print(tickets_not_approve)
    if tickets_not_approve is not None:
        for ticket in tickets_not_approve:
            try:
                tickets_approved.remove(ticket)
            except:
                pass

    """ get object in list tickets_approved """
    object_tickets = []
    for title_ticket in tickets_approved:
        obj_filtered = ApprovementTicketModels.objects.filter(checker__title=title_ticket)[0]
        object_tickets.append(obj_filtered)

    return object_tickets


def filter_tracker_ticket_helper(data_tickets):
    tickets = data_tickets.all()
    title_ticket_objs = []
    ticket_filtered = []

    """ append title tickets """
    for ticket in tickets:
        title_ticket_objs.append(ticket.title)

    """ append objs approvement """
    for title_ticket in title_ticket_objs:
        obj_filtered = ApprovementTicketModels.objects.filter(checker__title=title_ticket)[0]
        ticket_filtered.append(obj_filtered)

    return ticket_filtered
