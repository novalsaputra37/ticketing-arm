from django.db import models
from apps.accounts.models import UserModels


class TicketModels(models.Model):
    title = models.CharField(max_length=255)
    content = models.CharField(max_length=255)
    picture = models.ImageField(upload_to='support')
    author = models.ForeignKey(
        "accounts.UserModels",
        to_field="id",
        related_name="support_belong_user",
        on_delete=models.CASCADE,
    )
    send_to = models.ManyToManyField(UserModels, related_name="send_to_belong_user")
    checker = models.ManyToManyField(UserModels, related_name="checker_belong_user")
    status_ticket = models.BooleanField(default=False)
    info = models.JSONField(default=dict, blank=True, null=True)
    create = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"<{self.id}>. {self.title}"


class ApprovementTicketModels(models.Model):
    user = models.ForeignKey(
        "accounts.UserModels",
        to_field="id",
        related_name="approvement_belong_user",
        on_delete=models.CASCADE,
    )
    checker = models.ForeignKey(
        "ticket.TicketModels",
        to_field="id",
        related_name="checker_belong_checker_user",
        on_delete=models.CASCADE,
    )
    approve = models.BooleanField(default=False)
    info = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f"<{self.id}>. {self.user} - {self.checker}"
