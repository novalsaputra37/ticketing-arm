from django.urls import path
from apps.ticket import views

app_name = 'ticket'
urlpatterns = [
    path('', views.DashboardView.as_view(), name="dashboard"),

    # Ticket
    path('ticket/create/', views.CreateNewTicket.as_view(), name="create-new-ticket"),
    path('ticket/', views.ListTicketView.as_view(), name="list-ticket"),
    path('ticket/detail/<int:pk>', views.DetailTicketView.as_view(), name="detail-ticket"),
    path('ticket/update/<int:pk>', views.UpdateTicketView.as_view(), name="update-ticket"),
    path('ticlet/delete/<int:pk>', views.DeleteTicketView.as_view(), name="delete-ticket"),

    # Checker
    path('checker/', views.ListCheckerTicketView.as_view(), name="list-checker"),
    path('checker/detail/<int:pk>', views.DetailCheckerView.as_view(), name="detail-checker"),
    path('checker/update/<int:pk>', views.UpdateCheckerView.as_view(), name="update-checker"),
    path('checker/update-info/<int:pk>', views.UpdateInfoCheckerView.as_view(), name="update-info-checker"),

    #  Inbox
    path('inbox/', views.ListInboxView.as_view(), name="list-inbox"),
    path('inbox/detail/<int:pk>', views.DetailInboxView.as_view(), name="detail-inbox"),
    path('inbox/update/<int:pk>', views.UpdateInboxStatusTicketView.as_view(), name="update-inbox-status-ticket"),

    # Tracker
    path('tracker/', views.listTrackerView.as_view(), name="list-tracker-ticket"),
    path('tracker/detail/<int:pk>', views.ListDetailTrackerView.as_view(), name="list-detail-tracker-ticket")
]
