from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView, UpdateView, DetailView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.ticket.models import TicketModels, ApprovementTicketModels
from apps.ticket.forms import TicketForms, UpdateInfoTicketForms
from apps.ticket import helpers


class DashboardView(LoginRequiredMixin, TemplateView):
    login_required = True
    login_url = '/'
    template_name = "dashboard/dashboard.html"


# Ticket
class CreateNewTicket(LoginRequiredMixin, CreateView):
    model = TicketModels
    form_class = TicketForms
    success_url = reverse_lazy('ticket:list-ticket')
    template_name = "dashboard/ticket/create-ticket.html"

    def form_valid(self, form):
        response = super().form_valid(form)
        ticket = TicketModels.objects.order_by('-create')[0]
        helpers.create_approvement_models(ticket)
        return response


class ListTicketView(LoginRequiredMixin, ListView):
    model = TicketModels
    context_object_name = "tickets"
    template_name = "dashboard/ticket/list-ticket.html"

    def get_queryset(self):
        self.queryset = self.model.objects.filter(author=self.request.user)
        return super().get_queryset()


class DetailTicketView(LoginRequiredMixin, DetailView):
    model = TicketModels
    context_object_name = "ticket"
    template_name = "dashboard/ticket/detail-ticket.html"


class UpdateTicketView(LoginRequiredMixin, UpdateView):
    model = TicketModels
    form_class = TicketForms
    template_name = "dashboard/ticket/create-ticket.html"
    success_url = reverse_lazy('ticket:dashboard')

    def form_valid(self, form):
        response = super().form_valid(form)
        ticket = TicketModels.objects.get(pk=self.kwargs['pk'])
        helpers.filter_update_checker_helper(ticket)
        return response


class DeleteTicketView(LoginRequiredMixin, DeleteView):
    model = TicketModels
    context_object_name = "ticket"
    template_name = "dashboard/ticket/delete-ticket.html"
    success_url = reverse_lazy('ticket:list-ticket')


# Checker
class ListCheckerTicketView(LoginRequiredMixin, ListView):
    model = ApprovementTicketModels
    context_object_name = "checker_tickets"
    template_name = "dashboard/checker/list-checker.html"

    def get_queryset(self):
        self.queryset = self.model.objects.filter(user=self.request.user)
        return super().get_queryset()


class DetailCheckerView(LoginRequiredMixin, DetailView):
    model = ApprovementTicketModels
    context_object_name = "checker_tickets"
    template_name = "dashboard/checker/detail-checker.html"


class UpdateCheckerView(LoginRequiredMixin, UpdateView):
    model = ApprovementTicketModels
    fields = ["approve", "info"]
    template_name = "dashboard/ticket/create-ticket.html"
    success_url = reverse_lazy('ticket:list-checker')


class UpdateInfoCheckerView(LoginRequiredMixin, UpdateView):
    model = TicketModels
    form_class = UpdateInfoTicketForms
    template_name = "dashboard/checker/update-info-checker.html"

    def form_valid(self, form):
        get_ticket = self.model.objects.get(pk=self.kwargs['pk']).info
        helpers.massage_info_helper(self, form, get_ticket)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_ticket = self.model.objects.get(pk=self.kwargs['pk'])
        context["title"] = get_ticket.title
        try:
            context["info"] = get_ticket.info["info"]
        except:
            pass
        return context

    def get_success_url(self):
        return reverse_lazy('ticket:update-info-checker', kwargs={'pk': self.kwargs['pk']})

# Inbox
class ListInboxView(LoginRequiredMixin, ListView):
    model = ApprovementTicketModels
    template_name = "dashboard/inbox/list-inbox.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tickets = self.model.objects.filter(checker__send_to=self.request.user)
        context["data_tickets"] = helpers.filter_approve_ticket_helper(tickets)
        return context


class DetailInboxView(LoginRequiredMixin, DetailView):
    model = ApprovementTicketModels
    context_object_name = "inbox"
    template_name = "dashboard/inbox/detail-inbox.html"


class UpdateInboxStatusTicketView(LoginRequiredMixin, UpdateView):
    model = TicketModels
    fields = ["status_ticket", "info"]
    template_name = "dashboard/ticket/create-ticket.html"
    success_url = reverse_lazy('ticket:list-inbox')


# Tracker
class listTrackerView(LoginRequiredMixin, ListView):
    model = TicketModels
    template_name = "dashboard/tracker/list-tracker.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tickets = self.model.objects.filter(author=self.request.user)
        context['tracker_tickets'] = helpers.filter_tracker_ticket_helper(tickets)
        return context


class ListDetailTrackerView(LoginRequiredMixin, ListView):
    model = ApprovementTicketModels
    context_object_name = "tracker_tickets"
    template_name = "dashboard/tracker/list-detail-tracker.html"

    def get_queryset(self):
        filter_objs = self.model.objects.get(pk=self.kwargs['pk'])
        self.queryset = self.model.objects.filter(checker__title=filter_objs.checker.title)
        return super().get_queryset()
