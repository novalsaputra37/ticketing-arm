from django.contrib import admin
from apps.ticket.models import (TicketModels, ApprovementTicketModels)


@admin.register(TicketModels)
class SupportAdmin(admin.ModelAdmin):
    filter_horizontal = ("checker", "send_to")
    list_display = (
        'id',
        'title',
        'author',
        'create',
        'updated',
    )


@admin.register(ApprovementTicketModels)
class ApprovementTicket(admin.ModelAdmin):
    list_display = (
        'user',
        'checker',
        'approve',
        'info',
    )
