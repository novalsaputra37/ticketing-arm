from django import forms

from apps.ticket.models import TicketModels
from apps.accounts.models import UserModels


class TicketForms(forms.ModelForm):
    class Meta:
        model = TicketModels
        fields = ['title', 'content', 'picture', 'author', 'send_to', 'checker']

    title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "title",
            }
        )
    )

    content = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "placeholder": "content"
            }
        )
    )

    send_to = forms.ModelMultipleChoiceField(
        queryset=UserModels.objects.all(), widget=forms.CheckboxSelectMultiple()
    )

    checker = forms.ModelMultipleChoiceField(
        queryset=UserModels.objects.all(), widget=forms.CheckboxSelectMultiple()
    )


class UpdateInfoTicketForms(forms.ModelForm):
    class Meta:
        model = TicketModels
        fields = ["info"]

    info = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "placeholder": "info"
            }
        )
    )
