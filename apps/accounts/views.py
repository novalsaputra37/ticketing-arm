from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from apps.accounts.forms import LoginForms, RegisterForms


# Create your views here.
def login_user_view(request):
    forms = LoginForms(request.POST or None)
    msg = None

    if request.user.is_authenticated:
        return redirect("/dashboard")
    else:
        if request.method == "POST" and forms.is_valid():
            username = forms.cleaned_data.get("username")
            password = forms.cleaned_data.get("password")
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("/dashboard")
            else:
                msg = 'Invalid credentials'
        else:
            msg = 'Enter the correct username and password'

        return render(request, "authentication/login.html", {"forms": forms, "msg": msg})


def register_user_view(request):
    msg = None
    success = False
    forms = RegisterForms(request.POST)

    if request.method == "POST":
        forms = RegisterForms(request.POST)
        if forms.is_valid():
            forms.save()
            username = forms.cleaned_data.get("username")
            raw_password = forms.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)

            msg = 'User created - please <a href="/">login</a>.'
            success = True

            return redirect("/")

        else:
            msg = 'Form is not valid'
    else:
        forms = RegisterForms()

    return render(request, "authentication/register.html", {"forms": forms, "msg": msg, "success": success})
