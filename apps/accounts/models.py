from django.db import models
from django.contrib.auth.models import AbstractUser


class UserModels(AbstractUser):
    POSITION_CHOICES = (
        ("employee", "employee"),
        ("staff", "staff"),
        ("manager", "manager")
    )
    username = models.CharField(unique=True, max_length=50)
    position = models.CharField(choices=POSITION_CHOICES, max_length=50)
