from django.urls import path
from apps.accounts.views import login_user_view, register_user_view
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('', login_user_view, name="login"),
    path('register/', register_user_view, name="register"),
    path('logout/', LogoutView.as_view(), name="logout"),
]
